FROM ubuntu:20.04

RUN apt-get update --yes
RUN apt-get install --yes antlr4 libyaml-cpp-dev libantlr4-runtime-dev make g++
COPY . /app

RUN cd /app && make
