OUTPUT=output
GENERATED=generated
# runtime is where you downloaded and extracted https://www.antlr.org/download/antlr4-cpp-runtime-4.8-macos.zip
CCARGS=-g -c -isystem /usr/include/yaml-cpp/ -isystem /usr/include/antlr4-runtime/ -I $(GENERATED) -std=c++11 -Wno-deprecated
LDARGS=-g -lyaml-cpp
ifeq ("$(wildcard /usr/lib/libantlr4-runtime.so)","")
	LIBS=/usr/lib/x86_64-linux-gnu/libantlr4-runtime.a
else
	LIBS=/usr/lib/libantlr4-runtime.so
endif
ifneq ("$(wildcard /usr/lib/x86_64-linux-gnu/libyaml-cpp.so)","")
	LIBS+=/usr/lib/x86_64-linux-gnu/libyaml-cpp.so
endif
CC=g++
GRAMMAR=C
# This assumes you have antlr-4.8-complete.jar in the current directory.
#JAVA=/usr/bin/java
#ANTLR4=$(JAVA) -jar antlr-4.8-complete.jar
ANTLR4=antlr4

ANTLRGEN=Lexer Parser
OBJS=$(addsuffix .o,$(addprefix $(OUTPUT)/$(GRAMMAR),$(ANTLRGEN)))
GSOURCES=$(addsuffix .cpp,$(addprefix $(GENERATED)/$(GRAMMAR),$(ANTLRGEN)))

.precious: $(GSOURCES)

all: parser

test: parser
	./test/run_tests.sh

parser: dirs antlr4 parser.cpp $(OBJS) $(OUTPUT)/visitor.o $(OUTPUT)/listener.o $(OUTPUT)/features.o
	$(CC) $(CCARGS) parser.cpp  -o $(OUTPUT)/parser.o
	$(CC) $(LDARGS) $(OUTPUT)/parser.o $(OBJS) $(OUTPUT)/visitor.o $(OUTPUT)/listener.o $(OUTPUT)/features.o $(LIBS) -o parser

antlr4: $(GENERATED)/.generated;

$(GENERATED)/.generated: $(GRAMMAR).g4
	$(ANTLR4) -Dlanguage=Cpp -listener -visitor -o $(GENERATED) $(GRAMMAR).g4
	@touch $(GENERATED)/.generated

$(OUTPUT)/features.o: features.cpp
	$(CC) $(CCARGS) $< -o $@

$(OUTPUT)/visitor.o: visitor.cpp
	$(CC) $(CCARGS) $< -o $@

$(OUTPUT)/listener.o: listener.cpp
	$(CC) $(CCARGS) $< -o $@

$(OUTPUT)/%.o : $(GENERATED)/%.cpp
	$(CC) $(CCARGS) $< -o $@

$(GENERATED)/%.cpp: $(GENERATED)/.generated;

dirs:; mkdir -p $(OUTPUT) $(GENERATED)
clean:; rm -rf $(OUTPUT) $(GENERATED)
