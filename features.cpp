#include "features.h"
#include "visitor.h"
#include "listener.h"

#define COMMENT_CHANNEL 2
#define WHITESPACE_CHANNEL 1
#define DEFAULT_CHANNEL 0

#define NEWLINE 121
#define EOF -1

#define BLOCK_COMMENT 122
#define LINE_COMMENT 123

using namespace std;

struct line_length_data{
  size_t line;
  size_t line_length;
};

YAML::Emitter& operator << (YAML::Emitter& out, vector<indent_data>& v) {
  sort(v.begin(), v.end(), [](indent_data a, indent_data b){
    return a.line < b.line;
  });

  out << YAML::Key << "indent" << YAML::Value << YAML::BeginSeq;
  for( auto d : v ){
    out << YAML::BeginMap;
    out << YAML::Key << "line" << YAML::Value << (int)d.line;
    out << YAML::Key << "expected" << YAML::Value << d.expected;
    out << YAML::Key << "found" << YAML::Value << d.found;
    out << YAML::Key << "description" << YAML::Value << d.description;
    out << YAML::EndMap;
  }
  out << YAML::EndSeq;
  return out;
}

void report_line_lengths(vector<line_length_data> line_lengths,
                         YAML::Emitter &out,
                         int max_function_length){
  out << YAML::Key << "line_length" << YAML::Value << YAML::BeginSeq;
  for( auto d : line_lengths ){
    out << YAML::BeginMap;
    out << YAML::Key << "line" << YAML::Value << (int)d.line;
    out << YAML::Key << "line_length" << YAML::Value << (int)d.line_length;
    out << YAML::Key << "expected" << YAML::Value << max_function_length;
    out << YAML::EndMap;
  }
  out << YAML::EndSeq;
  return;
}

YAML::Emitter& operator << (YAML::Emitter& out, const vector<operator_data>& v) {
  out << YAML::Key << "operator_space" << YAML::Value << YAML::BeginSeq;
  for( auto d : v ){
    out << YAML::BeginMap;
    out << YAML::Key << "line" << YAML::Value << (int)d.line;
    out << YAML::Key << "char" << YAML::Value << (int)d.char_on_line;
    ostringstream description;
    description << "Space not found ";
    if (d.before){
      description << "before";
    } else {
      description << "after";
    }
    description << " the \"" << d.operator_str << "\" operator.";
    out << YAML::Key << "description" << YAML::Value << description.str();
    out << YAML::EndMap;
  }
  out << YAML::EndSeq;
  return out;
}

YAML::Emitter& operator << (YAML::Emitter& out, const vector<brace_issue>& v) {
  out << YAML::Key << "braces" << YAML::Value << YAML::BeginSeq;
  for( auto d : v ){
    out << YAML::BeginMap;
    out << YAML::Key << "line" << YAML::Value << (int)d.line;
    out << YAML::Key << "char" << YAML::Value << (int)d.char_on_line;
    out << YAML::Key << "description" << YAML::Value << d.description;
    out << YAML::EndMap;
  }
  out << YAML::EndSeq;
  return out;
}

int comments_check(vector<antlr4::Token*> comment_tokens,
                   int line_count, YAML::Emitter &out,
                   int min_comment_percentage){
  set<int> comment_lines;
  int line;

  for (auto token : comment_tokens) {
    auto type = token->getType();
    auto text = token->getText();

    switch(type){
      case BLOCK_COMMENT:
        line = token->getLine();
        comment_lines.insert(line);
        for(char character : text){
          if (character == '\n'){
            line++;
            comment_lines.insert(line);
          }
        }
        break;
      case LINE_COMMENT:
        comment_lines.insert(token->getLine());
        break;
      default:
        break;
    }
  }
  int comment_line_cnt = comment_lines.size();
  float comment_line_percentage = (comment_line_cnt*100) / line_count;
  if (comment_line_percentage < min_comment_percentage){
    out << YAML::Key << "comment_amount_exception" << YAML::BeginMap;
    out << YAML::Key << "comment_lines" << YAML::Value << comment_line_cnt;
    out << YAML::Key << "expected_percent" << YAML::Value << min_comment_percentage;
    out << YAML::Key << "found_percent" << YAML::Value << comment_line_percentage;
    out << YAML::EndMap;
    return 1;
  }
  return 0;
}

vector<struct line_length_data> line_length_check(vector<antlr4::Token*> tokens,
                                                  int max_chars_per_line){
  // Expand tabs to 4 spaces when counting length?
  int line;
  vector<struct line_length_data> data;

  for (auto token : tokens) {
    auto type = token->getType();

    if(type == NEWLINE && token->getCharPositionInLine() > max_chars_per_line){
      data.push_back({token->getLine(), token->getCharPositionInLine()});
    }
  }
  return data;
}

int report_function_lenghts(vector<function_length_data> function_lengths,
                            YAML::Emitter &out,
                            int max_function_length){
  auto it = begin(function_lengths);
  while (it != end(function_lengths)){
    cout << "delka:" << it->length << endl;
    if (it->length <= max_function_length){
      it = function_lengths.erase(it);
    } else {
      it++;
    }
  }
  if(function_lengths.empty()){
    return 0;
  }
  out << YAML::Key << "function_length" << YAML::Value << YAML::BeginSeq;
  for (auto function : function_lengths){
    out << YAML::BeginMap;
    out << YAML::Key << "function" << YAML::Value << function.function_name;
    out << YAML::Key << "line" << YAML::Value << function.line;
    out << YAML::Key << "length" << YAML::Value << function.length;
    out << YAML::Key << "expected" << YAML::Value << max_function_length;
    out << YAML::EndMap;
  }
  out << YAML::EndSeq;
  return 1;
}


void FeatureRunner::divide_tokens(){
  set<int> filtered_lines;
  for (auto token : token_stream->getTokens()) {
    int channel = token->getChannel();
    switch(channel){
      case DEFAULT_CHANNEL:
        tokens.push_back(token);
        filtered_lines.insert(token->getLine());
        if (token->getType() == EOF){
          line_count = token->getLine();
        }
        // consider having a line_count with only lines that have a non-whitespace
        // non-comment token on them?
        break;
      case WHITESPACE_CHANNEL:
        whitespace_tokens.push_back(token);
        break;
      case COMMENT_CHANNEL:
        comment_tokens.push_back(token);
        break;
      default:
        break;
    }
  }
  filtered_line_count = filtered_lines.size();
}

void FeatureRunner::filter_features(){
  for(auto it = feature_list.begin(); it != feature_list.end();){
    if (config[*it] && config[*it]["disable"] && config[*it]["disable"].as<bool>()){
      it = feature_list.erase(it);
    } else {
      ++it;
    }
  }
}

FeatureRunner::FeatureRunner(CParser &parser, YAML::Emitter &out, YAML::Node &config,
                             antlr4::CommonTokenStream &all_tokens){
  this->parser = &parser;
  this->out = &out;
  this->config = config;
  this->token_stream = &all_tokens;
  this->feature_list = {"indent",
                        "function_length",
                        "comments",
                        "braces",
                        "operator_spaces",
                        "line_length"};
  divide_tokens();
  filter_features();
}

int FeatureRunner::run_features(){
  int rc = 0;
  CParser::CompilationUnitContext* CompUnitCtx;
  antlr4::tree::ParseTree *tree;

  for(auto feature : feature_list){
    if (feature == "comments"){
      int comment_min_percentage = 20;
      if (config[feature] && config[feature]["min_percentage_lines"]){
        comment_min_percentage = config[feature]["min_percentage_lines"].as<int>();
      }
      if(comments_check(comment_tokens, line_count, *out, comment_min_percentage)){
        rc++;
      }
    } else if (feature == "function_length"){
      int function_length_max = 50;
      if (config[feature] && config[feature]["max"]){
        function_length_max = config[feature]["max"].as<int>();
      }
      FunctionLengthVisitor fn_len_visitor;
      CompUnitCtx = parser->compilationUnit();
      fn_len_visitor.visit(CompUnitCtx);
      if(report_function_lenghts(fn_len_visitor.function_lengths, *out,
                                 function_length_max)){
        rc++;
      }
      parser->reset();
    } else if (feature == "indent"){
      IndentationVisitor indent_visitor(token_stream, config);
      CompUnitCtx = parser->compilationUnit();
      indent_visitor.visit(CompUnitCtx);

      if(!indent_visitor.indent_changes.empty()){
        rc++;
        *out << indent_visitor.indent_changes;
      }
      parser->reset();
    } else if (feature == "braces"){
      BracesListener brace_listener(token_stream, config);
      tree = parser->compilationUnit();
      antlr4::tree::ParseTreeWalker::DEFAULT.walk(&brace_listener, tree);
      brace_listener.assessBracesStyle();
      if(!brace_listener.issues_found.empty()){
        rc++;
        *out << brace_listener.issues_found;
      }
      parser->reset();
    } else if (feature == "operator_spaces"){
      OperatorListener operator_listener(token_stream);
      tree = parser->compilationUnit();
      antlr4::tree::ParseTreeWalker::DEFAULT.walk(&operator_listener, tree);

      if(!operator_listener.issues_found.empty()){
        rc++;
        *out << operator_listener.issues_found;
      }
      parser->reset();
    } else if (feature == "line_length"){
      int max_length = 80;
      if (config[feature] && config[feature]["max"]){
        max_length = config[feature]["max"].as<int>();
      }
      vector<line_length_data> data = line_length_check(whitespace_tokens,
                                                        max_length);
      if (!data.empty()){
        report_line_lengths(data, *out, max_length);
        rc++;
      }
    }
  }

  return rc;
}
