#include <string>
#include <vector>
#include "antlr4-runtime.h"
#include "yaml-cpp/yaml.h"
#include "CBaseVisitor.h"
#include "CBaseListener.h"

class FeatureRunner {
  public:
    antlr4::CommonTokenStream *token_stream;
    std::vector<antlr4::Token*> whitespace_tokens;
    std::vector<antlr4::Token*> tokens;
    std::vector<antlr4::Token*> comment_tokens;
    std::list<std::string> feature_list;
    YAML::Emitter *out;
    YAML::Node config;
    CParser *parser;
    int line_count;
    int filtered_line_count;
    void divide_tokens();
    void filter_features();
    FeatureRunner(CParser &parser, YAML::Emitter &out, YAML::Node &config,
                  antlr4::CommonTokenStream &all_tokens);
    int run_features();
};
