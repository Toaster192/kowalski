#include "listener.h"
#define COLON 86
#define COMMA 88

using namespace std;

set<int> operators = {65 /* < */,
                      66 /* <= */,
                      67 /* < */,
                      68 /* >= */,
                      69 /* << */,
                      70 /* >> */,
                      71 /* + */,
                      73 /* - */,
                      76 /* / */,
                      77 /* % */,
                      78 /* & */,
                      79 /* | */,
                      80 /* && */,
                      81 /* || */,
                      82 /* ^ */,
                      85 /* ? */,
                      COLON /* : */,
                      COMMA /* , */,
                      89 /* = */,
                      90 /* *= */,
                      91 /* /= */,
                      92 /* %= */,
                      93 /* += */,
                      94 /* -= */,
                      95 /* <<= */,
                      96 /* >>= */,
                      97 /* &= */,
                      98 /* ^= */,
                      99 /* |= */,
                      100 /* == */,
                      101 /* != */};


antlr4::Token *getPreviousToken(antlr4::Token *token, antlr4::CommonTokenStream* token_stream){
  int index = token->getTokenIndex();
  return token_stream->getTokens()[index - 1];
}

antlr4::Token *getNextToken(antlr4::Token *token, antlr4::CommonTokenStream* token_stream){
  int index = token->getTokenIndex();
  return token_stream->getTokens()[index + 1];
}

antlr4::Token *getPreviousNonWhitespace(antlr4::Token *token, antlr4::CommonTokenStream* token_stream){
  int i = -1;
  antlr4::Token* prev_token;
  int token_index = token->getTokenIndex();
  auto tokens = token_stream->getTokens();

  while (token_index + i > 0){
    prev_token = tokens[token_index + i];
    if (prev_token->getChannel() == 0){
      return prev_token;
    }
    i--;
  }
  return nullptr;
}

antlr4::Token *getNextNonWhitespace(antlr4::Token *token, antlr4::CommonTokenStream* token_stream){
  int i = 1;
  antlr4::Token* next_token;
  int token_index = token->getTokenIndex();
  auto tokens = token_stream->getTokens();

  while (true){ // will stop at <EOF>
    next_token = tokens[token_index + i];
    if (next_token->getType() == -1){
      break;
    }
    if (next_token->getChannel() == 0){
      return next_token;
    }
    i++;
  }
  return nullptr;
}

OperatorListener::OperatorListener(antlr4::CommonTokenStream *stream){
  token_stream = stream;
}

void OperatorListener::checkSpacesAroundToken(antlr4::Token *token){
  if (getPreviousToken(token, this->token_stream)->getText() != " " &&
      token->getType() != COMMA){
    this->issues_found.insert(this->issues_found.begin(),
                              {token->getLine(), token->getCharPositionInLine(),
                               true, token->getText()});
  }
  if (getNextToken(token, this->token_stream)->getText() != " "){
    this->issues_found.insert(this->issues_found.begin(),
                              {token->getLine(), token->getCharPositionInLine(),
                               false, token->getText()});
  }
}

void OperatorListener::enterExpression(CParser::ExpressionContext * ctx){
  antlr4::Token *token = ctx->getStop();
  while (token != ctx->getStart()){
    if(operators.find(token->getType()) != operators.end()){
      checkSpacesAroundToken(token);
    }
    if (token->getType() == COMMA && ctx->expression()){
      // If we find a comma because we'd be processing an expression that will
      // be processed anyways (and check every operator twice)
      return;
    }
    token = getPreviousToken(token, this->token_stream);
  }
}

BracesListener::BracesListener(antlr4::CommonTokenStream *stream, YAML::Node config){
  this->token_stream = stream;
  this->config = config;
}

void BracesListener::filterBraces(int type, int style){
  // pass the combination of type / style to be filtered out as the "correct"
  // style
  vector<struct brace_data> result;
  for (auto d : braces_found){
    if (d.type != type || d.style != style){
      result.push_back(d);
    }
  }
  this->braces_found = result;
}

void BracesListener::assessBracesStyle(){
  // check if brace style isn't forced via config
  map<int, string> config_map;
  config_map[0] = "iteration_statement";
  config_map[1] = "selection_statement";
  config_map[2] = "function_definition";

  for(int i = 0; i < 3; i++){
    // check if brace style of specific fields isn't forced via config
    // enum brace_type {iterationSmt, selectionSmt, functionDef};
    if(config["braces"] && config["braces"][config_map[i]]){
      if (config["braces"][config_map[i]].as<string>() == "new_line"){
        filterBraces(i, new_line);
      } else {
        filterBraces(i, same_line);
      }
      continue;
    } else if(config["braces"] && config["braces"]["default"]){
      if (config["braces"]["default"].as<string>() == "new_line"){
        filterBraces(i, new_line);
      } else {
        filterBraces(i, same_line);
      }
      continue;
    }

    int nl = 0;
    int sl = 0;
    for(auto brace : this->braces_found){
      if(brace.type != i){
        continue;
      }
      if(brace.style == new_line){
        nl++;
      } else {
        sl++;
      }
    }
    if (nl > sl){
      filterBraces(i, new_line);
    } else {
      filterBraces(i, same_line);
    }
  }
  // Transform filterd braces into issues
  for (auto brace : braces_found){
    ostringstream description;
    description << "Braces inside a ";
    if (brace.type == iterationSmt){
      description << "iteration statement ";
    } else if (brace.type == functionDef){
      description << "function definition ";
    } else {
      description << "selection statement ";
    }
    description << "found ";
    if (brace.style == new_line){
      description << "on a new line";
    } else {
      description << "on the same line";
    }
    this->issues_found.push_back({brace.line, brace.char_on_line,
                                  description.str()});
  }
}

void BracesListener::processCompoundStatement(CParser::CompoundStatementContext * ctx,
                                                brace_type type,
                                                bool check_else = false){
  antlr4::Token *leftBrace = ctx->getStart();
  antlr4::Token *rightBrace = ctx->getStop();

  if(getPreviousNonWhitespace(leftBrace, this->token_stream)->getLine() == leftBrace->getLine()){
    this->braces_found.push_back({leftBrace->getLine(), leftBrace->getCharPositionInLine(),
                                    type, same_line});
  } else if (getPreviousNonWhitespace(leftBrace, this->token_stream)->getLine() + 1 == leftBrace->getLine()) {
    this->braces_found.push_back({leftBrace->getLine(), leftBrace->getCharPositionInLine(),
                                    type, new_line});
  } else {
    this->issues_found.push_back({leftBrace->getLine(), leftBrace->getCharPositionInLine(),
                                  string("Found extra lines before left brace")});
  }

  if(getNextNonWhitespace(leftBrace, this->token_stream) != nullptr &&
      getNextNonWhitespace(leftBrace, this->token_stream)->getLine() == leftBrace->getLine()){
    this->issues_found.push_back({leftBrace->getLine(), leftBrace->getCharPositionInLine(),
                                  string("Found a token on the same line after a left brace")});
  }

  if(getPreviousNonWhitespace(rightBrace, this->token_stream)->getLine() == rightBrace->getLine()){
    this->issues_found.push_back({rightBrace->getLine(), rightBrace->getCharPositionInLine(),
                                  string("Found a token on the same line before a right brace")});
  }
  if (check_else){
    auto else_token = getNextNonWhitespace(rightBrace, this->token_stream);
  } else {
    if(getNextNonWhitespace(rightBrace, this->token_stream) != nullptr &&
        getNextNonWhitespace(rightBrace, this->token_stream)->getLine() == rightBrace->getLine()){
      this->issues_found.push_back({rightBrace->getLine(), rightBrace->getCharPositionInLine(),
                                    string("Found a token on the same line after a right brace")});
    }
  }
}

void BracesListener::enterIterationStatement(CParser::IterationStatementContext * ctx){
  if (ctx->statement()->compoundStatement()){
    processCompoundStatement(ctx->statement()->compoundStatement(), iterationSmt);
  } else {
    auto token = ctx->statement()->getStart();
    this->issues_found.push_back({token->getLine(), token->getCharPositionInLine(),
                                  string("Statement without braces")});
  }
}

void BracesListener::enterSelectionStatement(CParser::SelectionStatementContext * ctx){
  auto statements = ctx->statement();
  if (statements.size() == 1){
    if (statements[0]->compoundStatement()){
      processCompoundStatement(statements[0]->compoundStatement(), selectionSmt);
    } else {
      auto token = statements[0]->getStart();
      this->issues_found.push_back({token->getLine(), token->getCharPositionInLine(),
                                    string("Statement without braces")});
    }
  } else {
    // if else
    if (statements[0]->compoundStatement()){
      processCompoundStatement(statements[0]->compoundStatement(), selectionSmt, true);
    } else {
      auto token = statements[0]->getStart();
      this->issues_found.push_back({token->getLine(), token->getCharPositionInLine(),
                                    string("Statement without braces")});
    }
    if (statements[1]->compoundStatement()){
      processCompoundStatement(statements[1]->compoundStatement(), selectionSmt);
    } else {
      auto token = statements[1]->getStart();
      this->issues_found.push_back({token->getLine(), token->getCharPositionInLine(),
                                    string("Statement without braces")});
    }
  }
}

void BracesListener::enterFunctionDefinition(CParser::FunctionDefinitionContext * ctx){
  processCompoundStatement(ctx->compoundStatement(), functionDef);
}
