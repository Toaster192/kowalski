#include <string>
#include <vector>
#include "antlr4-runtime.h"
#include "yaml-cpp/yaml.h"
#include "CBaseListener.h"

struct operator_data{
  size_t line;
  size_t char_on_line;
  bool before;
  std::string operator_str;
};

class  OperatorListener : public CBaseListener {
public:
  antlr4::CommonTokenStream *token_stream;
  std::vector<operator_data> issues_found;
  OperatorListener(antlr4::CommonTokenStream *stream);
  void checkSpacesAroundToken(antlr4::Token *token);
  void enterExpression(CParser::ExpressionContext * ctx);
};


enum brace_type {iterationSmt, selectionSmt, functionDef};
enum brace_style {new_line, same_line};

struct brace_data{
  size_t line;
  size_t char_on_line;
  brace_type type;
  brace_style style;
};

struct brace_issue{
  size_t line;
  size_t char_on_line;
  std::string description;
};

class  BracesListener : public CBaseListener {
  public:
    antlr4::CommonTokenStream *token_stream;
    YAML::Node config;
    std::vector<brace_data> braces_found;
    std::vector<brace_issue> issues_found;
    // void checkSpacesAroundToken(antlr4::Token *token);
    void filterBraces(int type, int style);
    void assessBracesStyle();
    BracesListener(antlr4::CommonTokenStream *stream, YAML::Node config);
    void processCompoundStatement(CParser::CompoundStatementContext * ctx,
                                  brace_type type, bool check_else);
    void enterIterationStatement(CParser::IterationStatementContext * ctx);
    void enterSelectionStatement(CParser::SelectionStatementContext * ctx);
    void enterFunctionDefinition(CParser::FunctionDefinitionContext * ctx);
};
