#include <iostream>
#include <fstream>
#include <strstream>
#include <string>
#include <unistd.h>
#include "yaml-cpp/yaml.h"
#include "antlr4-runtime.h"
#include "CLexer.h"
#include "CParser.h"
#include "features.h"

using namespace std;

// Not really needed as all these variables
// have defaults before trying to read from
// config? or the other way around, use this
// as defaults when the config doesn't contain
// these values (so that the defaults are all
// at one place)
const char* default_config =
"function_length:\n"
"- max: 50\n"
"comments:\n"
"- min_percentage_lines: 20\n"
"line_length:\n"
"- max: 80\n";

class MyParserErrorListener: public antlr4::BaseErrorListener {
  virtual void syntaxError(
      antlr4::Recognizer *recognizer,
      antlr4::Token *offendingSymbol,
      size_t line,
      size_t charPositionInLine,
      const string &msg,
      exception_ptr e) override {
    ostrstream s;
    s << "Line(" << line << ":" << charPositionInLine << ") Error(" << msg << ")";
    throw invalid_argument(s.str());
  }
};

int main(int argc, char *argv[]) {
  string config_name = "";
  string output_name = "output.yaml";
  int index;
  int c;

  opterr = 0;

  while ((c = getopt(argc, argv, "c:o:")) != -1){
    switch (c)
      {
      case 'c':
        config_name = optarg;
        break;
      case 'o':
        output_name = optarg;
        break;
      case '?':
        if (optopt == 'c' || optopt == 'o')
          fprintf(stderr, "Option -%c requires an argument.\n", optopt);
        else if(isprint (optopt))
          fprintf(stderr, "Unknown option `-%c'.\n", optopt);
        else
          fprintf(stderr,
                  "Unknown option character `\\x%x'.\n",
                  optopt);
        return 1;
      default:
        abort();
      }
  }

  ofstream output_file(output_name);
  YAML::Emitter out;
  YAML::Node config;
  if (config_name != ""){
    config = YAML::LoadFile(config_name);
  } else {
    try{
      config = YAML::LoadFile("config.yaml");
    }
    catch (YAML::BadFile e){
      config = YAML::Load(default_config);
    }
  }
  if (optind != argc - 1){
    cerr << "Argument error." << endl;
    return 1;
  }

  if (config["scanner"]){
    config = config["scanner"];
  }

  ifstream stream;
  char *input_filename = argv[optind];
  stream.open(input_filename);
  if (!stream){
    cout << "Input file not found" << endl;
    return 11;
  }
  antlr4::ANTLRInputStream input(stream);
  CLexer lexer(&input);
  antlr4::CommonTokenStream all_tokens(&lexer);
  all_tokens.fill();

  MyParserErrorListener errorListner;

  CParser parser(&all_tokens);
  parser.removeErrorListeners();
  parser.addErrorListener(&errorListner);

  int rc = 0;

  try {
    FeatureRunner runner(parser, out, config, all_tokens);
    int line_count = runner.line_count;
    int filtered_line_count = runner.filtered_line_count;
    out << YAML::BeginMap;
    out << YAML::Key << "meta" << YAML::BeginMap;
    out << YAML::Key << "line_count" << YAML::Value << line_count;
    out << YAML::Key << "filtered_line_count" << YAML::Value << filtered_line_count;
    out << YAML::Key << "filename" << YAML::Value << input_filename;
    out << YAML::EndMap;

    rc = runner.run_features();

    out << YAML::EndSeq;
    cout << out.c_str() << endl;
    output_file << out.c_str() << endl;
    output_file.close();
  } catch (invalid_argument &e) {
    cout << e.what() << endl;
    return 10;
  }
  return rc;
}
