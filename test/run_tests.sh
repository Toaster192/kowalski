#!/usr/bin/env bash

parser="../parser"

total=0
succeeded=0

[ -d "./test/" ] && cd "./test/"

for test_input in tests/*.c; do
    [ -e "$test_input" ] || continue
    test_config="${test_input%.*}.config"
    test_output="${test_input%.*}.yaml"
    if ! [ -e "$test_output" ]; then
        echo "test $test_input is missing expected output"
        continue
    fi
    if [ -e "$test_config" ]; then
        $parser "$test_input" -c "$test_config" >/dev/null
    else
        $parser "$test_input" >/dev/null
    fi
    ((total++))
    diff=$(diff output.yaml $test_output)
    if [ -z "$diff" ] ; then
        echo "test $test_input passed"
        ((succeeded++))
    else
        echo "\ntest $test_input failed,"
        echo "diff:\n$diff"
    fi
done

echo " "
echo "Testing results: $succeeded / $total"

exit $((total - succeeded))
