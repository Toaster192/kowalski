#include "visitor.h"
#include <cmath>
#include <algorithm>
#define OPENING_BRACKET 63
#define WHITESPACE 120
#define NEWLINE 121

using namespace std;


antlrcpp::Any FunctionLengthVisitor::visit(CParser::CompilationUnitContext *ctx) {
  if (ctx->translationUnit()){
    return visitTranslationUnit(ctx->translationUnit());
  }
  return 0;
}

antlrcpp::Any FunctionLengthVisitor::visitTranslationUnit(CParser::TranslationUnitContext *ctx) {
  visitExternalDeclaration(ctx->externalDeclaration());
  if (ctx->translationUnit()){
    visitTranslationUnit(ctx->translationUnit());
  }
  return 0;
}

antlrcpp::Any FunctionLengthVisitor::visitExternalDeclaration(CParser::ExternalDeclarationContext *ctx) {
  if (ctx->functionDefinition()){
    visitFunctionDefinition(ctx->functionDefinition());
  }
  return 0;
}

antlrcpp::Any FunctionLengthVisitor::visitFunctionDefinition(CParser::FunctionDefinitionContext *ctx) {
  int start_line = ctx->getStart()->getLine();
  int end_line = ctx->getStop()->getLine();
  CParser::DirectDeclaratorContext* node = ctx->declarator()->directDeclarator();
  while(!node->Identifier()){
    if(node->directDeclarator()){
      node = node->directDeclarator();
    } else {
      // This should only happen in case of macros
      this->function_lengths.push_back({start_line, end_line - start_line,
                                        "Error finding function name"});
      return 0;
    }
  }
  this->function_lengths.push_back({start_line, end_line - start_line,
                                    node->Identifier()->getText()});
  return 0;
}

antlr4::Token *IndentationVisitor::findToken(antlr4::Token *start, const char *text,
                                             antlr4::Token *end){
  antlr4::Token* token = start;
  auto tokens = this->token_stream->getTokens();
  int index = token->getTokenIndex();
  while(token!=end){
    if (token->getText() == text){
      return token;
    }
    index++;
    token = tokens[index];
  }
  return nullptr;
}

antlr4::Token *IndentationVisitor::getLastTokenOnLine(antlr4::Token *token){
  antlr4::Token* prev_token;
  int index = token->getTokenIndex();
  if (index == 0){
    return nullptr;
  }
  prev_token = this->token_stream->getTokens()[index - 1];
  if (token->getLine() == prev_token->getLine()){
    return prev_token;
  }
  return nullptr;
}

antlr4::Token *IndentationVisitor::getLastNonWhitespace(antlr4::Token *token){
  int i = -1;
  antlr4::Token* prev_token;
  while (token->getTokenIndex() + i > 0){
    prev_token = this->token_stream->getTokens()[token->getTokenIndex() + i];
    if (prev_token->getChannel() == 0){
      return prev_token;
    }
    i--;
  }
  return nullptr;
}

void IndentationVisitor::loadIndentMultiplier(){
  map<int, int> indents;
  int last = 0;
  int indent = 0;
  int width;

  for (auto token : this->indent_tokens){
    auto text = token->getText();
    width = text.length();
    indent = abs(width - last);
    if (indent >= 1){
      if(indents.find(indent) == indents.end()){
        indents[indent] = 1;
      } else {
        indents[indent] = indents[indent] + 1;
      }
    }
    last = width;
  }

  int max = 0;
  for (auto pair : indents){
    int count = indents[pair.first];
    if (count > max){
      max = count;
      indent = pair.first;
    }
  }
  cout << "indent_multiplier: " << indent << endl;
  this->indent_multiplier = indent;
}

void IndentationVisitor::loadIndentChar(){
  int tabs = 0;
  int spaces = 0;

  for (antlr4::Token *token : this->indent_tokens) {
    auto text = token->getText();
    for (auto c : text) {
      if (c == ' '){
        spaces++;
      } else if(c == '\t'){
        tabs++;
      }
    }
  }
  if (tabs*4 > spaces){
    cout << "More than 20% of indent characters were tabs, assuming tabs as indent character" << endl;
    this->indent_char = '\t';
  } else{
    cout << "Assuming spaces as indent character" << endl;
    this->indent_char = ' ';
  }
}

void IndentationVisitor::loadIndentTokens(){
  bool indent = true;

  for (auto token : this->token_stream->getTokens()) {
    auto type = token->getType();
    switch(type){
      case WHITESPACE:
        if (indent){
          this->indent_tokens.push_back(token);
        }
        break;
      case NEWLINE:
        indent = true;
        break;
      default:
        indent = false;
        break;
    }
  }
}

vector<int> IndentationVisitor::checkWhitespaceOnEmptyLines(){
  int indent = 0;
  bool blank_line = true;
  vector<int> lines;

  for (auto token : this->token_stream->getTokens()) {
    auto type = token->getType();
    auto text = token->getText();
    switch(type){
      case WHITESPACE:
        if (!blank_line){
          break;
        }
        indent++;
        break;
      case NEWLINE:
        if (blank_line && indent > 0){
          this->indent_changes.push_back({token->getLine(), "",
                                          getLastTokenOnLine(token)->getText(),
                                          "Whitespace on empty line"});
          lines.push_back(token->getLine());
        }
        blank_line = true;
        indent = 0;
        break;
      default:
        blank_line = false;
        break;
    }
  }
  return lines;
}

void IndentationVisitor::checkWhitespaceBeforeEOL(vector<int> ignore={}){
  int indent = 0;
  bool blank_line = true;
  vector<string> excess;

  for (auto token : this->token_stream->getTokens()) {
    if(find(ignore.begin(), ignore.end(), token->getLine()) != ignore.end()){
      continue;
    }
    auto type = token->getType();
    auto text = token->getText();
    switch(type){
      case WHITESPACE:
        excess.push_back(text);
        break;
      case NEWLINE:
        if (!excess.empty()){
          string excess_string("");
          for (string str : excess){
            excess_string.append(str);
          }
          this->indent_changes.push_back({token->getLine(), "",
                                          excess_string,
                                          "Excess whitespace before \\n"});
        }
        excess.clear();
        break;
      default:
        excess.clear();
        break;
    }
  }
}

antlrcpp::Any IndentationVisitor::checkIndent(antlr4::ParserRuleContext *ctx){
  antlr4::Token* start = ctx->getStart();
  return checkIndent(start);
}

antlrcpp::Any IndentationVisitor::checkIndent(antlr4::Token *start){
  antlr4::Token* prev_token;
  int line = 0;
  if(start->getType() == OPENING_BRACKET){
    prev_token = getLastNonWhitespace(start);
    if (prev_token->getLine() == start->getLine()){
      return 0;
    }
  }
  prev_token = getLastTokenOnLine(start);
  if(prev_token == nullptr){
    if(this->indent_level != 0){
      this->indent_changes.push_back({start->getLine(), string(this->indent_multiplier * this->indent_level, this->indent_char),
                                      "", "Line not indented"});
      cout << "line: " << start->getLine() << " not indented, expected "
           << this->indent_level << " indentation levels" << endl;
    }
    return 0;
  }
  if (getLastTokenOnLine(prev_token)!=nullptr){
    //cout << "line: " << start->getLine() << " unexpected token before token: "
    //     << start->getText() << endl;
    return 0;
  }
  auto indent_text = prev_token->getText();
  if(indent_text != string(this->indent_multiplier * this->indent_level, this->indent_char)){
    this->indent_changes.push_back({start->getLine(), string(this->indent_multiplier * this->indent_level, this->indent_char),
                                    indent_text, "Line not indented as expected"});
    cout << "line: " << start->getLine() << " indented wrong, expected "
         << this->indent_level << " indentation levels" << endl;
    cout << "indent_multiplier: " << this->indent_multiplier << " delka indent_text: " << indent_text.length() << endl;
  }
  return 0;
}

IndentationVisitor::IndentationVisitor(antlr4::CommonTokenStream *stream, YAML::Node config){
  token_stream = stream;
  loadIndentTokens();
  if(config["indent"] && config["indent"]["char"]){
    this->indent_char = config["indent"]["char"].as<char>();
  } else{
    loadIndentChar();
  }
  if(config["indent"] && config["indent"]["multiplier"]){
    this->indent_multiplier = config["indent"]["multiplier"].as<int>();
  } else{
    loadIndentMultiplier();
  }
  if(config["indent"] && config["indent"]["switch_label_indent"]){
    this->switch_statements_level = config["indent"]["switch_label_indent"].as<int>();
  } else {
    cout << "switch_label_indent is not specified" << endl
         << "Will assume based on the first case I find" << endl;
    this->switch_statements_level = -1;
  }
  vector<int> lines = checkWhitespaceOnEmptyLines();
  checkWhitespaceBeforeEOL(lines);
}

antlrcpp::Any IndentationVisitor::visit(CParser::CompilationUnitContext *ctx) {
  this->indent_level = 0;
  if (ctx->translationUnit()){
    return visitTranslationUnit(ctx->translationUnit());
  }
  return 0;
}

antlrcpp::Any IndentationVisitor::visitTranslationUnit(CParser::TranslationUnitContext *ctx) {
  visitExternalDeclaration(ctx->externalDeclaration());
  if (ctx->translationUnit()){
    visitTranslationUnit(ctx->translationUnit());
  }
  return 0;
}

antlrcpp::Any IndentationVisitor::visitExternalDeclaration(CParser::ExternalDeclarationContext *ctx) {
  if (ctx->functionDefinition()){
    visitFunctionDefinition(ctx->functionDefinition());
  }
  return 0;
}

antlrcpp::Any IndentationVisitor::visitFunctionDefinition(CParser::FunctionDefinitionContext *ctx) {
  checkIndent(ctx);
  this->indent_level++;
  visitCompoundStatement(ctx->compoundStatement(), 0);
  this->indent_level--;
  return 0;
}

antlrcpp::Any IndentationVisitor::visitCompoundStatement(CParser::CompoundStatementContext *ctx,
                                                         int level_increase=0) {
  // check position of { and }, expect them to be lower than the statements
  this->indent_level--;
  checkIndent(ctx->getStart());
  checkIndent(ctx->getStop());
  this->indent_level++;

  if (level_increase == -1){ // if we're on our first switch and haven't figured out a switch level yet
    antlr4::Token* token = ctx->getStart();
    int token_index = token->getTokenIndex();
    auto tokens = token_stream->getTokens();
    int i = 1;

    while (true){
      token = tokens[token_index + i];
      if (token->getType() == -1){
        break;
      }
      if (token->getChannel() == 0){
        break;
      }
      i++;
    }

    cout << token->getText() << endl;
    token = tokens[token->getTokenIndex() - 1];
    if(token->getText() == string(this->indent_multiplier * (this->indent_level - 1), this->indent_char)){
      // case on same level as switch
      this->switch_statements_level = 0;
      level_increase = 0;
      cout << "Assuming cases on the same level as switch" << endl;
      cout << "foo: \"" << token->getText() << "\"" << endl;
      cout << "bar: \"" << string(this->indent_multiplier * (this->indent_level - 1), this->indent_char) << "\"" << endl;
    } else {
      this->switch_statements_level = 1;
      level_increase = 1;
      cout << "Assuming cases on a different level as switch" << endl;
      cout << "foo: \"" << token->getText() << "\"" << endl;
      cout << "bar: \"" << string(this->indent_multiplier * (this->indent_level - 1), this->indent_char) << "\"" << endl;
    }
  }

  this->indent_level+=level_increase;
  if(ctx->blockItemList()){
    visitBlockItemList(ctx->blockItemList());
  }
  this->indent_level-=level_increase;
  return 0;
}

antlrcpp::Any IndentationVisitor::visitBlockItemList(CParser::BlockItemListContext *ctx) {
  visitBlockItem(ctx->blockItem());
  if(ctx->blockItemList()){
    visitBlockItemList(ctx->blockItemList());
  }
  return 0;
}

antlrcpp::Any IndentationVisitor::visitBlockItem(CParser::BlockItemContext *ctx) {
  if(ctx->statement()){
    visitStatement(ctx->statement());
  } else {
    checkIndent(ctx);
    visitChildren(ctx);
  }
  return 0;
}

antlrcpp::Any IndentationVisitor::visitStatement(CParser::StatementContext *ctx) {
  if(ctx->labeledStatement()){
    visitLabeledStatement(ctx->labeledStatement());
  } else if(ctx->selectionStatement()){
    visitSelectionStatement(ctx->selectionStatement());
  } else if(ctx->iterationStatement()){
    visitIterationStatement(ctx->iterationStatement());
  } else if(ctx->compoundStatement()){
    visitCompoundStatement(ctx->compoundStatement(), 0);
  } else {
    checkIndent(ctx);
    visitChildren(ctx);
  }
  return 0;
}

antlrcpp::Any IndentationVisitor::visitSelectionStatement(CParser::SelectionStatementContext *ctx) {
  antlr4::Token* start = ctx->getStart();
  checkIndent(start);
  this->indent_level++;
  if (start->getText() == "switch"){
    if(ctx->statement()[0]->compoundStatement()){
      visitCompoundStatement(ctx->statement()[0]->compoundStatement(),
                             this->switch_statements_level);
    } else {
      visitChildren(ctx);
    }
  } else {
    if (!ctx->statement().empty()){
      visitStatement(ctx->statement()[0]);
    }
    antlr4::Token* token = findToken(start, "else", ctx->getStop());
    if (token != nullptr){
      this->indent_level--;
      checkIndent(token);
      this->indent_level++;
      if (!ctx->statement().empty() && ctx->statement().size() > 1){
        if (ctx->statement()[1]->selectionStatement()){
          this->indent_level--;
          visitStatement(ctx->statement()[1]);
          return 0;
        } else {
          visitStatement(ctx->statement()[1]);
        }
      }
    }
  }
  this->indent_level--;
  return 0;
}

antlrcpp::Any IndentationVisitor::visitIterationStatement(CParser::IterationStatementContext *ctx) {
  antlr4::Token* start = ctx->getStart();
  checkIndent(start);
  this->indent_level++;
  visitStatement(ctx->statement());
  this->indent_level--;
  if (start->getText() == "do"){
    antlr4::Token* token = findToken(start, "while", ctx->getStop());
    if (token != nullptr){
      checkIndent(token);
    }
  }
  return 0;
}

antlrcpp::Any IndentationVisitor::visitLabeledStatement(CParser::LabeledStatementContext *ctx) {
  antlr4::Token* start = ctx->getStart();
  if (start->getText() == "case" || start->getText() == "default"){
    this->indent_level--; // reduce indent_level as cases should be lower than the statements
    checkIndent(start);
    this->indent_level++;
    visitChildren(ctx);
  } else {
    checkIndent(start);
    visitChildren(ctx);
  }
  return 0;
}
