#include <string>
#include <vector>
#include "antlr4-runtime.h"
#include "yaml-cpp/yaml.h"
#include "CBaseVisitor.h"

struct indent_data{
  size_t line;
  std::string expected;
  std::string found;
  std::string description;
};

struct function_length_data{
  int line;
  int length;
  std::string function_name;
};

class FunctionLengthVisitor : CBaseVisitor {
  public:
    std::vector<function_length_data> function_lengths;
    antlrcpp::Any visit(CParser::CompilationUnitContext *ctx);
    antlrcpp::Any visitTranslationUnit(CParser::TranslationUnitContext *ctx);
    antlrcpp::Any visitExternalDeclaration(CParser::ExternalDeclarationContext *ctx);
    antlrcpp::Any visitFunctionDefinition(CParser::FunctionDefinitionContext *ctx);
};

class IndentationVisitor : CBaseVisitor {
  public:
    antlr4::CommonTokenStream *token_stream;
    std::vector<indent_data> indent_changes;
    std::vector<antlr4::Token*> indent_tokens;
    int indent_multiplier;
    char indent_char;
    int indent_level;
    int switch_statements_level;
    void loadIndentMultiplier();
    void loadIndentChar();
    void loadIndentTokens();
    std::vector<int> checkWhitespaceOnEmptyLines();
    void checkWhitespaceBeforeEOL(std::vector<int> ignore);
    IndentationVisitor(antlr4::CommonTokenStream *stream, YAML::Node config);
    antlr4::Token *getLastTokenOnLine(antlr4::Token *token);
    antlr4::Token *getLastNonWhitespace(antlr4::Token *token);
    antlr4::Token *findToken(antlr4::Token *start, const char *text,
			     antlr4::Token *end);
    void indentationPrecheck(antlr4::CommonTokenStream &tokens, antlr4::dfa::Vocabulary &vocab);
    antlrcpp::Any checkIndent(antlr4::ParserRuleContext *ctx);
    antlrcpp::Any checkIndent(antlr4::Token *start);
    antlrcpp::Any visit(CParser::CompilationUnitContext *ctx);
    antlrcpp::Any visitTranslationUnit(CParser::TranslationUnitContext *ctx);
    antlrcpp::Any visitExternalDeclaration(CParser::ExternalDeclarationContext *ctx);
    antlrcpp::Any visitFunctionDefinition(CParser::FunctionDefinitionContext *ctx);
    antlrcpp::Any visitCompoundStatement(CParser::CompoundStatementContext *ctx,
					 int level_increase);
    antlrcpp::Any visitBlockItemList(CParser::BlockItemListContext *ctx);
    antlrcpp::Any visitBlockItem(CParser::BlockItemContext *ctx);
    antlrcpp::Any visitStatement(CParser::StatementContext *ctx);
    antlrcpp::Any visitSelectionStatement(CParser::SelectionStatementContext *ctx);
    antlrcpp::Any visitIterationStatement(CParser::IterationStatementContext *ctx);
    antlrcpp::Any visitLabeledStatement(CParser::LabeledStatementContext *ctx);
};
